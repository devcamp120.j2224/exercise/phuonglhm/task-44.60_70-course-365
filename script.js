var gCoursesDB = {
    description: "This DB includes all courses in system",
    courses: [
        {
            id: 1,
            courseCode: "FE_WEB_ANGULAR_101",
            courseName: "How to easily create a website with Angular",
            price: 750,
            discountPrice: 600,
            duration: "3h 56m",
            level: "Beginner",
            coverImage: "images/courses/course-angular.jpg",
            teacherName: "Morris Mccoy",
            teacherPhoto: "images/teacher/morris_mccoy.jpg",
            isPopular: false,
            isTrending: true
        },
        {
            id: 2,
            courseCode: "BE_WEB_PYTHON_301",
            courseName: "The Python Course: build web application",
            price: 1050,
            discountPrice: 900,
            duration: "4h 30m",
            level: "Advanced",
            coverImage: "images/courses/course-python.jpg",
            teacherName: "Claire Robertson",
            teacherPhoto: "images/teacher/claire_robertson.jpg",
            isPopular: false,
            isTrending: true
        },
        {
            id: 5,
            courseCode: "FE_WEB_GRAPHQL_104",
            courseName: "GraphQL: introduction to graphQL for beginners",
            price: 850,
            discountPrice: 650,
            duration: "2h 15m",
            level: "Intermediate",
            coverImage: "images/courses/course-graphql.jpg",
            teacherName: "Ted Hawkins",
            teacherPhoto: "images/teacher/ted_hawkins.jpg",
            isPopular: true,
            isTrending: false
        },
        {
            id: 6,
            courseCode: "FE_WEB_JS_210",
            courseName: "Getting Started with JavaScript",
            price: 550,
            discountPrice: 300,
            duration: "3h 34m",
            level: "Beginner",
            coverImage: "images/courses/course-javascript.jpg",
            teacherName: "Ted Hawkins",
            teacherPhoto: "images/teacher/ted_hawkins.jpg",
            isPopular: true,
            isTrending: true
        },
        {
            id: 8,
            courseCode: "FE_WEB_CSS_111",
            courseName: "CSS: ultimate CSS course from beginner to advanced",
            price: 750,
            discountPrice: 600,
            duration: "3h 56m",
            level: "Beginner",
            coverImage: "images/courses/course-css.jpg",
            teacherName: "Juanita Bell",
            teacherPhoto: "images/teacher/juanita_bell.jpg",
            isPopular: true,
            isTrending: true
        },
        {
            id: 14,
            courseCode: "FE_WEB_WORDPRESS_111",
            courseName: "Complete Wordpress themes & plugins",
            price: 1050,
            discountPrice: 900,
            duration: "4h 30m",
            level: "Advanced",
            coverImage: "images/courses/course-wordpress.jpg",
            teacherName: "Clevaio Simon",
            teacherPhoto: "images/teacher/clevaio_simon.jpg",
            isPopular: true,
            isTrending: false
        }
    ]
}

var gCoursePopular = {
        description: "This DB includes all courses popular in system",
        courses: []
}

var gCourseTrending = {
    description: "This DB includes all courses trending in system",
    courses: []
}

//R2: gán/thực thi sự kiện cho elements
$(document).ready(function(){
    loadPopularCourse()
    loadTrendingCourse()
})

//R3: khai báo hàm xử lý sự kiện

function loadPopularCourse() {
    console.log("loading Popular course!")
    coursePopular()
    console.log(gCoursePopular.courses)
    //debugger;
    for(var bI=0; bI<gCoursePopular.courses.length;bI++) {
        var bCol = ""
        bCol += `<div class="col-sm-3">
                    <div class="card">
                        <div class="card-header p-0">`;
        bCol += `<img class="card-img-top mx-auto d-block" src=` + gCoursePopular.courses[bI].coverImage + `>`
        bCol += `</div>
                <div class="card-body">
                <a href=""><h6 class="card-title">`+ gCoursePopular.courses[bI].courseName + `</h6></a>`
        bCol += `<span><i class="far fa-clock"></i></span>  <span>`+ gCoursePopular.courses[bI].duration + `</span>`           
        bCol += `<span>` + gCoursePopular.courses[bI].level + `</span>` 
        bCol += `<p class="card-text"> $<span>` + gCoursePopular.courses[bI].discountPrice + ` </span>`
        bCol += `<span style="text-decoration:line-through" class="text-success">$<i> `+ gCoursePopular.courses[bI].price + `</i></span></p></div>`
        bCol += `<div class="card-footer">
                <img class="rounded-circle img-thumbnail" src=` + gCoursePopular.courses[bI].teacherPhoto + ` width="50" height="50">`
        bCol += `<span class="pl-3">` + gCoursePopular.courses[bI].teacherName + `</span>`
        bCol += `<span><i class="far fa-bookmark float-right"></i></span>
                    </div>
                </div>
            </div> `    
        $(".popular-course").append(bCol)       
    }
}

function loadTrendingCourse() {
    console.log("loading Trending course!")
    courseTrending()
    console.log(gCourseTrending.courses)
    //debugger;
    for(var bI=0; bI<gCourseTrending.courses.length;bI++) {
        var bCol = ""
        bCol += `<div class="col-sm-3">
                    <div class="card">
                        <div class="card-header p-0">`;
        bCol += `<img class="card-img-top mx-auto d-block" src=` + gCourseTrending.courses[bI].coverImage + `>`
        bCol += `</div>
                <div class="card-body">
                <a href=""><h6 class="card-title">`+ gCourseTrending.courses[bI].courseName + `</h6></a>`
        bCol += `<span><i class="far fa-clock"></i></span>  <span>`+ gCourseTrending.courses[bI].duration + `</span>`           
        bCol += `<span>` + gCourseTrending.courses[bI].level + `</span>` 
        bCol += `<p class="card-text"> $<span>` + gCourseTrending.courses[bI].discountPrice + ` </span>`
        bCol += `<span style="text-decoration:line-through" class="text-success">$<i> `+ gCourseTrending.courses[bI].price + `</i></span></p></div>`
        bCol += `<div class="card-footer">
                <img class="rounded-circle img-thumbnail" src=` + gCourseTrending.courses[bI].teacherPhoto + ` width="50" height="50">`
        bCol += `<span class="pl-3">` + gCourseTrending.courses[bI].teacherName + `</span>`
        bCol += `<span><i class="far fa-bookmark float-right"></i></span>
                    </div>
                </div>
            </div> `    
        $(".trending-course").append(bCol)       
    }
}
//B4: khai báo hàm dùng chung
function coursePopular() {
    var vCoursePopularArr = gCoursePopular.courses;
    for(var bI = 0; bI<gCoursesDB.courses.length;bI++) {
        if(gCoursesDB.courses[bI].isPopular == true) {
            vCoursePopularArr.push(gCoursesDB.courses[bI])
        }
    }
    return vCoursePopularArr
}

function courseTrending() {
    var vCourseTrendingArr = gCourseTrending.courses;
    for(var bI = 0; bI<gCoursesDB.courses.length;bI++) {
        if(gCoursesDB.courses[bI].isTrending == true) {
            vCourseTrendingArr.push(gCoursesDB.courses[bI])
        }
    }
    return vCourseTrendingArr
}